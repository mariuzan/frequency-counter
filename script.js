    //document.getElementById("countButton").onclick= function ()
    
    function pegaLetra(){
        let typedText= document.getElementById("textInput").value;
        typedText=typedText.toLowerCase();
        typedText=typedText.replace(/[^a-z'\s]+/g, "");        
        const letterCounts={};                
            for(let i=0;i<typedText.length;i++){
                currentLetter=typedText[i];
                if(letterCounts[currentLetter]===undefined){
                        letterCounts[currentLetter]=1;
                        
                }else{
                        letterCounts[currentLetter]++;                        
                }             
                              
            }            
            return letterCounts;
        }
    function pegaPalavra(){
        let typedText=document.getElementById("textInput").value;
        typedText=typedText.toLowerCase();
        typedText=typedText.replace(/[^a-z'\s]+/g, "");
        words = typedText.split(/\s/);
        const wordCounts={};
            for(let i=0;i<words.length;i++){
                currentWord=words[i];
                if(wordCounts[currentWord]===undefined){
                    wordCounts[currentWord]=1;
                }
                else{
                    wordCounts[currentWord]++;
                }                 
            }
            return wordCounts;
    }
    
    
    function contarLetras(letterCounts){
        for(let letter in letterCounts){            
            const span=document.createElement("span");
            const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", "); 
            span.appendChild(textContent); 
            document.getElementById("lettersDiv").appendChild(span);

        }

    }
    function contarPalavras(wordCounts){
        for(let word in wordCounts){            
            const span=document.createElement("span");
            const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", "); 
            span.appendChild(textContent); 
            document.getElementById("wordsDiv").appendChild(span);

        }

    }